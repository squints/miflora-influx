#!/usr/bin/python3

from miflora.miflora_poller import MiFloraPoller, MI_CONDUCTIVITY, MI_MOISTURE, MI_LIGHT, MI_TEMPERATURE, MI_BATTERY
from btlewrap import BluepyBackend
import time
import datetime
import psutil
from gpiozero import CPUTemperature
from influxdb import InfluxDBClient
import json
import os

def main():
    try:
        __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(__location__, 'conf.json')) as f:
            data = json.load(f)
    except Exception as e:
        print ("Something went wrong trying to parse the config file: {1}".format(e.strerror))
        print ("Aborting...")
        exit(1)
    
    try:
        ifclient = InfluxDBClient(host=data["server"], port=data["port"], username=data["user"], password=data["pass"], database=data["db"])
    except Exception as e:
        print ("Something went wrong trying to connect to InfluxDB: {1}".format(e.strerror))
        print ("Aborting...")
        ifclient.close()
        exit(1)

    try:
        while True:
            poller = MiFloraPoller(data["MAC"], BluepyBackend)
            print("Getting data from Mi Flora")
            print("FW: {}".format(poller.firmware_version()))
            print("Name: {}".format(poller.name()))
            print("Temperature: {}".format((1.8*poller.parameter_value(MI_TEMPERATURE))+32))
            print("Moisture: {}".format(poller.parameter_value(MI_MOISTURE)))
            print("Light: {}".format(poller.parameter_value(MI_LIGHT)))
            print("Conductivity: {}".format(poller.parameter_value(MI_CONDUCTIVITY)))
            print("Battery: {}".format(poller.parameter_value(MI_BATTERY)))
            disk = psutil.disk_usage('/')
            mem = psutil.virtual_memory()
            load = psutil.cpu_percent(interval=None)
            temp = CPUTemperature().temperature
            print("Getting HW data")
            print("CPU %: {}".format(load))
            print("RAM %: {}".format(mem.percent))
            print("HDD %: {}".format(disk.percent))
            print("CPUTemp in (C): {}".format(temp))
            print("Sending to Influx")
            body = [
                {
                    "measurement": data["measurement_name"],
                    "time": datetime.datetime.utcnow(),
                    "fields": {
                        "Temperature": ((1.8*poller.parameter_value(MI_TEMPERATURE))+32),
                        "Moisture": poller.parameter_value(MI_MOISTURE),
                        "Light": poller.parameter_value(MI_LIGHT),
                        "Conductivity": poller.parameter_value(MI_CONDUCTIVITY),
                        "Battery": poller.parameter_value(MI_BATTERY),
                        "CPU": load,
                        "RAM": mem.percent,
                        "HDD": disk.percent,
                        "CPUTemp": temp,
                    }
                }
            ]
            ifclient.write_points(body)
            print("Sleeping " + str(data["delay"]) + "s...")
            time.sleep(data["delay"])
    except (KeyboardInterrupt, SystemExit):
        print("Killing Daemon...")
        ifclient.close()
        exit(0)
    except Exception as e:
        print ("Something went wrong trying to connect to InfluxDB: {1}".format(e.strerror))
        print ("Aborting...")
        ifclient.close()
        exit(1)

if __name__ == '__main__':
    main()
